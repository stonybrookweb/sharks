(function(global){
"use strict";
// Map for Shark Data made with Google Maps API https://developers.google.com/maps/.

// Initialize all variables that will be used on the map.
var largeInfowindow,
	map,
	mapLatLng,
	sharksMapUrl,
	sharkSelectedImage,
	sharkDataPointImage,
	sealMarkerImage,
	styles;

// Set Initial view to Chatham, MA.
mapLatLng = { lat: 41.6336683, lng: -70.053201 };

// Set images into variables.
sharksMapUrl = "https://maps.googleapis.com/maps/api/js?key=AIzaSyCq2cIVmSSzyc3K_OO2VTIxSvHKhZfwnFU";
sharkSelectedImage = 'https://www.bu.edu/bostonia/wpassets/issues/fall17/tracking-cape-cod-sharks/images/shark-selected.png';
sharkDataPointImage = 'https://www.bu.edu/bostonia/wpassets/issues/fall17/tracking-cape-cod-sharks/images/shark-marker.png';
sealMarkerImage = 'https://www.bu.edu/bostonia/wpassets/issues/fall17/tracking-cape-cod-sharks/images/seal-marker.png';

// Create a new blank array for all the map markers.
global.markers = [];

global.initMap  = function () {
    if( 'undefined' === typeof google ){
        console.log('Google Maps failed to load. Please try again later.');
        return;
	}

    // Create the map.
    map = new google.maps.Map( document.getElementById('map'), {
        center: mapLatLng,
        zoom: 9,
        styles: styles,
		streetViewControl: false,
        mapTypeControl: false,
		fullscreenControl: false
    });
	drawSeals();
	setupMap( katharine );
	};

	// Custom style from Snazzy Maps https://snazzymaps.com/ .
	styles =
	[
	    {
	        "featureType": "administrative.country",
	        "elementType": "geometry.fill",
	        "stylers": [
	            {
	                "visibility": "on"
	            },
	            {
	                "saturation": "47"
	            },
	            {
	                "lightness": "100"
	            },
	            {
	                "gamma": "10.00"
	            },
	            {
	                "color": "#3a3434"
	            },
	            {
	                "weight": "8.68"
	            }
	        ]
	    },
	    {
	        "featureType": "administrative.country",
	        "elementType": "geometry.stroke",
	        "stylers": [
	            {
	                "visibility": "on"
	            },
	            {
	                "hue": "#ff0000"
	            },
	            {
	                "saturation": "26"
	            },
	            {
	                "gamma": "0.00"
	            },
	            {
	                "weight": "1.29"
	            }
	        ]
	    },
	    {
	        "featureType": "administrative.country",
	        "elementType": "labels.text",
	        "stylers": [
	            {
	                "visibility": "on"
	            },
	            {
	                "hue": "#008eff"
	            },
	            {
	                "gamma": "0.00"
	            }
	        ]
	    },
	    {
	        "featureType": "administrative.country",
	        "elementType": "labels.text.fill",
	        "stylers": [
	            {
	                "visibility": "on"
	            },
	            {
	                "hue": "#8400ff"
	            },
	            {
	                "weight": "0.01"
	            }
	        ]
	    },
	    {
	        "featureType": "administrative.province",
	        "elementType": "geometry.stroke",
	        "stylers": [
	            {
	                "gamma": "10.00"
	            }
	        ]
	    },
	    {
	        "featureType": "administrative.province",
	        "elementType": "labels.text.fill",
	        "stylers": [
	            {
	                "visibility": "on"
	            },
	            {
	                "hue": "#ff0000"
	            },
	            {
	                "gamma": "0.00"
	            },
	            {
	                "weight": "0.87"
	            }
	        ]
	    },
	    {
	        "featureType": "poi.attraction",
	        "elementType": "labels.text",
	        "stylers": [
	            {
	                "visibility": "off"
	            }
	        ]
	    },
	    {
	        "featureType": "poi.business",
	        "elementType": "labels.text",
	        "stylers": [
	            {
	                "visibility": "off"
	            }
	        ]
	    },
	    {
	        "featureType": "poi.business",
	        "elementType": "labels.icon",
	        "stylers": [
	            {
	                "visibility": "off"
	            }
	        ]
	    },
	    {
	        "featureType": "road",
	        "elementType": "geometry",
	        "stylers": [
	            {
	                "visibility": "on"
	            }
	        ]
	    },
	    {
	        "featureType": "road",
	        "elementType": "labels",
	        "stylers": [
	            {
	                "visibility": "off"
	            }
	        ]
	    },
	    {
	        "featureType": "road.highway",
	        "elementType": "geometry.stroke",
	        "stylers": [
	            {
	                "visibility": "on"
	            }
	        ]
	    },
	    {
	        "featureType": "road.highway",
	        "elementType": "labels",
	        "stylers": [
	            {
	                "visibility": "off"
	            }
	        ]
	    },
	    {
	        "featureType": "road.highway",
	        "elementType": "labels.text",
	        "stylers": [
	            {
	                "visibility": "off"
	            }
	        ]
	    },
	    {
	        "featureType": "road.highway.controlled_access",
	        "elementType": "geometry.fill",
	        "stylers": [
	            {
	                "visibility": "on"
	            }
	        ]
	    },
	    {
	        "featureType": "road.arterial",
	        "elementType": "geometry.fill",
	        "stylers": [
	            {
	                "visibility": "off"
	            },
	            {
	                "color": "#9c9dae"
	            }
	        ]
	    },
	    {
	        "featureType": "road.arterial",
	        "elementType": "labels",
	        "stylers": [
	            {
	                "visibility": "off"
	            }
	        ]
	    },
	    {
	        "featureType": "road.local",
	        "elementType": "labels",
	        "stylers": [
	            {
	                "visibility": "off"
	            }
	        ]
	    },
	    {
	        "featureType": "transit.station.rail",
	        "elementType": "geometry.stroke",
	        "stylers": [
	            {
	                "lightness": "35"
	            },
	            {
	                "visibility": "simplified"
	            },
	            {
	                "weight": "0.15"
	            }
	        ]
	    },
	    {
	        "featureType": "water",
	        "elementType": "geometry.fill",
	        "stylers": [
	            {
	                "visibility": "on"
	            },
	            {
	                "lightness": "4"
	            },
	            {
	                "weight": "0.01"
	            },
	            {
	                "color": "#192340"
	            }
	        ]
	    }
	];

/*
 * Function to place markers for shark data into the global markers array and various actions.
 */
function setupMap( initialLocations ){
	var title = 'Katharine,<br>the Great White,<br>';
	largeInfowindow = new google.maps.InfoWindow();

	// This loop uses the shark lng + lat data to create new markers for each point.
	for ( var i = 0; i < initialLocations.length; i++ ) {
		// Get the position and timestamp data.
		var positionObject = {};
		positionObject.lat = initialLocations[i].lat;
		positionObject.lng = initialLocations[i].lng;
		var position = positionObject;
		var sightingTimestamp = initialLocations[i].timestamp;

		// Create a marker per location, and put into markers array.
		var marker = new google.maps.Marker({
			position: position,
			title: title,
			timestamp: sightingTimestamp,
			map: map,
			icon: sharkDataPointImage,
			id: i,
			status: false,
			shark: true
		});

		// Push the marker to our array of markers.
		markers.push(marker);

		// Add event listener to open the large infowindow at each marker.
		marker.addListener( 'click', function() {
			if ( marker.status ) {
				// Marker has already been selected, this is a close click.
				deselectShark ( this );
			} else {
				// Marker was not open when it was clicked.
				// Check all global markers first and set all to false and change icon.
				for ( var sharkMarker in markers ){
					if ( markers[sharkMarker].shark ){
						deselectShark( markers[sharkMarker] );
					}
				}
				populateInfoWindow( this, largeInfowindow );
				selectedShark( this );
				map.panTo( this.getPosition() ); // Center selected marker when clicked.
			}
		});
	} // End for Loop
}

/*
 * This function populates the infowindow when the marker is clicked.
 * Only one infowindow is allowed at a time.
 */
function populateInfoWindow( marker, infowindow ) {
	var sharkDate = new Date( marker.timestamp * 1000 ).toLocaleString('en-US');
    var sharkData = '<p>Date: ' + sharkDate + ' UTC</p>';
	sharkData += '<p>At:<br> Latitude: ' + marker.getPosition().lat().toFixed( 5 ) + '</br>';
	sharkData += 'Longitude: ' + marker.getPosition().lng().toFixed( 5 ) + '</br></div>';

	// Check to make sure the infowindow is not already opened on this marker.
    if (infowindow.marker != marker) {
        infowindow.setContent('');
        infowindow.marker = marker;
        // Add listener to clear the infowindow marker when infowindow is closed.
        infowindow.addListener('closeclick', function() {
			deselectShark( infowindow.marker );
			infowindow.marker = null;
        });

		infowindow.setContent('<div class="infowindow"><h3>' + marker.title + ' Surfaced On</h3>' + sharkData + '</div>');
        // Open infowindow on the marker.
        infowindow.open(map, marker);
    }
} // End populate infowindow

/*
 * Function to draw seal markers based on data set.
 */
function drawSeals(){
	for ( var location in seals ) {
		var marker = new google.maps.Marker({
			position: seals[location].center,
			map: map,
			icon: sealMarkerImage
		});
		markers.push( marker );
	}
}

/*
 * Function to change the icon to selectedShark.
 */
function selectedShark ( marker ){
	marker.setIcon( sharkSelectedImage );
	marker.status = true;
}

/*
 * Function to change the icon to sharkDataPointImage.
 */
function deselectShark ( marker ){
	if( null != marker ){
		marker.setIcon( sharkDataPointImage );
		marker.status = false;
	}
}

/*
 * Make map responsive and fit all markers on resize.
 */
window.onresize = function() {
    map.setCenter( mapLatLng );
};

global.googleError = function() {
    alert('Google Maps Failed to load. Please try again later.');
};

jQuery.getScript( sharksMapUrl, initMap );



// Start Overlay control
jQuery( document ).ready( function( $ ) {
	var transEndEventNames,
		transEndEventName,
		support,
		container = jQuery('.container'),
		triggerBttn = jQuery('a.trigger-overlay'),
		overlay = jQuery('.overlay'),
		closeBttn = jQuery('a.overlay-close');

	transEndEventNames = {
		'WebkitTransition': 'webkitTransitionEnd',
		'MozTransition': 'transitionend',
		'OTransition': 'oTransitionEnd',
		'msTransition': 'MSTransitionEnd',
		'transition': 'transitionend'
	},
	transEndEventName = transEndEventNames[Modernizr.prefixed('transition')],
	support = {transitions:Modernizr.csstransitions};

	function toggleOverlay(type){

		var overlay = jQuery('.overlay.'+type);
		if(overlay.hasClass('open')){
			overlay.removeClass('open');
			jQuery( "body" ).removeClass( "modal-open" );
			container.removeClass('overlay-open');
			overlay.addClass('close');
			var onEndTransitionFn = function(ev){
				if(support.transitions){
					if(ev.propertyName !== 'visibility') return;
					this.removeEventListener(transEndEventName,onEndTransitionFn);
				}
				overlay.removeClass('close');
			};
			if(support.transitions){
				jQuery(overlay).on(transEndEventName,onEndTransitionFn);
			} else {
				onEndTransitionFn();
			}
		} else {
				overlay.addClass('open');
				container.addClass('overlay-open');
			 jQuery( "body" ).addClass( "modal-open" );
		}
	}
	triggerBttn.on( 'click', function(e){
		e.preventDefault();
		var gettype = jQuery(this).attr('href').substr(1);
		toggleOverlay(gettype);
		if ( typeof( map ) ){
			map.setCenter( mapLatLng );
			map.setZoom( 9 );
		}
	});
	closeBttn.on( 'click', function(e){
		e.preventDefault();
		var gettype = jQuery(this).attr('href').substr(1);
		toggleOverlay(gettype);
		if ( typeof( map ) ){
			largeInfowindow.close();
			deselectShark( largeInfowindow.marker );
		}
	});
});

}(window));



var seals = {
	highhead: {
		center: { lat: 42.0647045, lng: -70.114878 },
		name: 'High Head, Provincetown'
	},
	longpoint: {
		center: { lat: 42.0270151, lng: -70.1807548 },
 		name: 'Long Point, Provincetown'
	},
	nauset: {
		center: { lat: 41.7884596, lng: -69.9374866 },
		name: 'Chatham near Coast Guard and Nauset Beaches'
	},
	monomoy: {
		center: { lat: 41.6368484, lng: -69.9813052 },
		name: 'Monomoy'
	},
	greatpoint: {
		center: { lat: 41.390372, lng: -70.0520577 },
		name: 'Great Point on Nantucket'
	},
	muskeget: {
		center: { lat: 41.3355036, lng: -70.4396632} ,
		name: 'Muskeget Island near Nantucket'
	},
	nomans: {
		center: { lat: 41.2570345, lng: -70.825753 },
		name: 'Nomans Land on Martha’s Vineyard'
	}
};

var katharine = [
{ lat: 41.63600159 , lng: -69.91101074, timestamp: 1376255401 },
{ lat: 41.64899826 , lng: -69.93701172, timestamp: 1376256668 },
{ lat: 41.64899826 , lng: -69.92999268, timestamp: 1376267980 },
{ lat: 41.49100113 , lng: -70.01901245, timestamp: 1377092546 },
{ lat: 41.44300079 , lng: -70.11898804, timestamp: 1377160565 },
{ lat: 41.50500107 , lng: -70.00299072, timestamp: 1377275627 },
{ lat: 41.46300125 , lng: -70.0710144, timestamp: 1377341305 },
{ lat: 41.43299866 , lng: -70.06900024, timestamp: 1377447575 },
{ lat: 41.45199966 , lng: -70.07998657, timestamp: 1377525277 },
{ lat: 41.41699982 , lng: -69.90600586, timestamp: 1377568602 },
{ lat: 41.50600052 , lng: -69.98199463, timestamp: 1377651100 },
{ lat: 41.45399857 , lng: -69.98699951, timestamp: 1377772491 },
{ lat: 41.45100021 , lng: -70.37799072, timestamp: 1377833373 },
{ lat: 41.49100113 , lng: -70.0329895, timestamp: 1378398580 },
{ lat: 41.40599823 , lng: -70.19900513, timestamp: 1378435812 },
{ lat: 41.36199951 , lng: -70.11801147, timestamp: 1378548446 },
{ lat: 41.43399811 , lng: -70.1499939, timestamp: 1378605760 },
{ lat: 41.4659996 , lng: -70.09500122, timestamp: 1378690726 },
{ lat: 41.43799973 , lng: -70.00500488, timestamp: 1378807249 },
{ lat: 41.43799973 , lng: -70.02398682, timestamp: 1378909860 },
{ lat: 41.45399857 , lng: -70.00299072, timestamp: 1378986579 },
{ lat: 41.44200134 , lng: -70.12200928, timestamp: 1379525609 },
{ lat: 41.47399902 , lng: -70.11499023, timestamp: 1379602058 },
{ lat: 41.58800125 , lng: -69.87600708, timestamp: 1383328653 },
{ lat: 41.5019989 , lng: -69.89401245, timestamp: 1383414249 },
{ lat: 41.63100052 , lng: -70.67001343, timestamp: 1383527417 },
{ lat: 41.17300034 , lng: -70.32800293, timestamp: 1385628110 },
{ lat: 40.92499924 , lng: -69.91598511, timestamp: 1385891663 },
{ lat: 41.01900101 , lng: -69.95599365, timestamp: 1385945908 },
{ lat: 41.27099991 , lng: -69.60900879, timestamp: 1386322983 },
{ lat: 40.97600174 , lng: -70.16799927, timestamp: 1386378584 },
{ lat: 40.55500031 , lng: -70.9960022, timestamp: 1386463234 },
{ lat: 40.06800079 , lng: -72.27099609, timestamp: 1386548100 },
{ lat: 39.02199936 , lng: -73.66101074, timestamp: 1386635856 },
{ lat: 38.0909996 , lng: -74.64001465, timestamp: 1386721457 },
{ lat: 37.22000122 , lng: -75.38800049, timestamp: 1386807584 },
{ lat: 36.07300186 , lng: -75.63299561, timestamp: 1386894747 },
{ lat: 35.5 , lng: -75.09799194, timestamp: 1386984205 },
{ lat: 35.07500076 , lng: -75.66900635, timestamp: 1387067049 },
{ lat: 34.73300171 , lng: -76.29199219, timestamp: 1387152079 },
{ lat: 33.35800171 , lng: -77.50500488, timestamp: 1387268450 },
{ lat: 33.77500153 , lng: -77.30300903, timestamp: 1387332777 },
{ lat: 33.40499878 , lng: -77.6960144, timestamp: 1387411439 },
{ lat: 32.79399872 , lng: -78.32800293, timestamp: 1387533227 },
{ lat: 32.55199814 , lng: -78.69198608, timestamp: 1387588449 },
{ lat: 32.38100052 , lng: -79.38400269, timestamp: 1387675503 },
{ lat: 32.24100113 , lng: -80.19900513, timestamp: 1387761392 },
{ lat: 31.5170002 , lng: -79.88800049, timestamp: 1387855517 },
{ lat: 31.44799995 , lng: -80.8500061, timestamp: 1387930706 },
{ lat: 30.60199928 , lng: -81.14199829, timestamp: 1388046283 },
{ lat: 30.45299911 , lng: -81.18798828, timestamp: 1388139935 },
{ lat: 30.35099983 , lng: -80.79199219, timestamp: 1388195436 },
{ lat: 30.06399918 , lng: -81.04299927, timestamp: 1388280532 },
{ lat: 29.14900017 , lng: -80.44900513, timestamp: 1388522381 },
{ lat: 29.11100006 , lng: -80.37298584, timestamp: 1388544597 },
{ lat: 29.29500008 , lng: -80.63400269, timestamp: 1388749137 },
{ lat: 29.23999977 , lng: -80.4670105, timestamp: 1388817525 },
{ lat: 29.1989994 , lng: -80.73999023, timestamp: 1389059821 },
{ lat: 29.15200043 , lng: -80.70199585, timestamp: 1389150283 },
{ lat: 28.86000061 , lng: -80.55801392, timestamp: 1389262204 },
{ lat: 28.77899933 , lng: -80.25100708, timestamp: 1389318630 },
{ lat: 28.63599968 , lng: -80.48800659, timestamp: 1389406569 },
{ lat: 28.53899956 , lng: -80.43200684, timestamp: 1389492650 },
{ lat: 28.95100021 , lng: -80.51599121, timestamp: 1390043961 },
{ lat: 28.93899918 , lng: -80.07800293, timestamp: 1390131772 },
{ lat: 29.06100082 , lng: -80.43499756, timestamp: 1390210030 },
{ lat: 29.12299919 , lng: -80.48498535, timestamp: 1390293295 },
{ lat: 29.375 , lng: -80.47799683, timestamp: 1390990485 },
{ lat: 29.50200081 , lng: -80.98400879, timestamp: 1391077468 },
{ lat: 29.51199913 , lng: -80.9630127, timestamp: 1391206922 },
{ lat: 29.17700005 , lng: -80.67498779, timestamp: 1391445721 },
{ lat: 29.17099953 , lng: -80.66000366, timestamp: 1391517700 },
{ lat: 29.43400002 , lng: -81.04501343, timestamp: 1391633130 },
{ lat: 29.38400078 , lng: -80.9460144, timestamp: 1391687627 },
{ lat: 30.33600044 , lng: -81.25900269, timestamp: 1392074034 },
{ lat: 30.34499931 , lng: -81.25900269, timestamp: 1392082459 },
{ lat: 30.92200089 , lng: -81.31900024, timestamp: 1392738409 },
{ lat: 30.95100021 , lng: -81.24899292, timestamp: 1392822763 },
{ lat: 30.80500031 , lng: -81.46398926, timestamp: 1393298589 },
{ lat: 30.75699997 , lng: -81.36999512, timestamp: 1393425259 },
{ lat: 30.75699997 , lng: -81.34799194, timestamp: 1393724713 },
{ lat: 30.25600052 , lng: -81.23400879, timestamp: 1394382881 },
{ lat: 30.26099968 , lng: -81.04299927, timestamp: 1394465788 },
{ lat: 30.47699928 , lng: -81.27801514, timestamp: 1395101166 },
{ lat: 30.54100037 , lng: -81.26300049, timestamp: 1395338116 },
{ lat: 30.38500023 , lng: -81.43099976, timestamp: 1395388331 },
{ lat: 30.54599953 , lng: -81.36499023, timestamp: 1395539872 },
{ lat: 30.63199997 , lng: -81.35800171, timestamp: 1395624961 },
{ lat: 30.8409996 , lng: -81.31900024, timestamp: 1396450927 },
{ lat: 30.68700027 , lng: -81.41101074, timestamp: 1396622599 },
{ lat: 30.68400002 , lng: -81.38400269, timestamp: 1396658286 },
{ lat: 30.53899956 , lng: -81.28799438, timestamp: 1396747085 },
{ lat: 30.18499947 , lng: -81.29800415, timestamp: 1396828847 },
{ lat: 31.18099976 , lng: -81.20999146, timestamp: 1397233853 },
{ lat: 31.0739994 , lng: -81.33898926, timestamp: 1397302051 },
{ lat: 31.35400009 , lng: -81.17401123, timestamp: 1397399114 },
{ lat: 31.32600021 , lng: -81.05999756, timestamp: 1397446282 },
{ lat: 31.27499962 , lng: -81.2460022, timestamp: 1397528870 },
{ lat: 30.88599968 , lng: -81.17700195, timestamp: 1398277241 },
{ lat: 31.14500046 , lng: -81.10900879, timestamp: 1398334347 },
{ lat: 31.2140007 , lng: -81.19900513, timestamp: 1398384799 },
{ lat: 31.45999908 , lng: -81.18798828, timestamp: 1398509704 },
{ lat: 31.66200066 , lng: -80.95700073, timestamp: 1398610131 },
{ lat: 31.45100021 , lng: -80.50698853, timestamp: 1398694999 },
{ lat: 31.46999931 , lng: -80.69900513, timestamp: 1398737410 },
{ lat: 31.53899956 , lng: -80.75601196, timestamp: 1398821012 },
{ lat: 31.62100029 , lng: -80.63101196, timestamp: 1398938653 },
{ lat: 31.43000031 , lng: -81.1000061, timestamp: 1398994485 },
{ lat: 30.78100014 , lng: -81.29400635, timestamp: 1399222412 },
{ lat: 30.22800064 , lng: -81.06600952, timestamp: 1399391795 },
{ lat: 29.90699959 , lng: -80.8789978, timestamp: 1399479489 },
{ lat: 29.50399971 , lng: -80.89001465, timestamp: 1399558483 },
{ lat: 29.15699959 , lng: -80.83200073, timestamp: 1399620044 },
{ lat: 28.72800064 , lng: -80.46099854, timestamp: 1399716614 },
{ lat: 28.60199928 , lng: -80.45901489, timestamp: 1399771351 },
{ lat: 27.89100075 , lng: -80.06900024, timestamp: 1399894063 },
{ lat: 27.82900047 , lng: -81.43798828, timestamp: 1399948073 },
{ lat: 26.53000069 , lng: -79.74798584, timestamp: 1400170705 },
{ lat: 26.1970005 , lng: -79.89001465, timestamp: 1400241654 },
{ lat: 26.02000046 , lng: -79.8710022, timestamp: 1400286147 },
{ lat: 25.7310009 , lng: -79.94500732, timestamp: 1400376529 },
{ lat: 25.1779995 , lng: -79.85699463, timestamp: 1400483697 },
{ lat: 25.0340004 , lng: -79.90899658, timestamp: 1400551136 },
{ lat: 24.35700035 , lng: -80.52700806, timestamp: 1400702098 },
{ lat: 24.36400032 , lng: -80.58200073, timestamp: 1400757166 },
{ lat: 24.30200005 , lng: -80.91799927, timestamp: 1401074250 },
{ lat: 24.36800003 , lng: -81.36999512, timestamp: 1401155670 },
{ lat: 24.23699951 , lng: -82.21798706, timestamp: 1401265268 },
{ lat: 24.13999939 , lng: -82.47799683, timestamp: 1401347112 },
{ lat: 24.04100037 , lng: -82.92401123, timestamp: 1401416840 },
{ lat: 24.19400024 , lng: -83.12799072, timestamp: 1401508716 },
{ lat: 24.33499908 , lng: -83.82598877, timestamp: 1401622132 },
{ lat: 24.51799965 , lng: -84.15200806, timestamp: 1401668326 },
{ lat: 25.12000084 , lng: -86.43200684, timestamp: 1401997874 },
{ lat: 25.51600075 , lng: -85.49398804, timestamp: 1402070256 },
{ lat: 25.60700035 , lng: -85.28500366, timestamp: 1402124844 },
{ lat: 25.84000015 , lng: -84.58700562, timestamp: 1402277908 },
{ lat: 26.27599907 , lng: -84.48699951, timestamp: 1402402492 },
{ lat: 27.21299934 , lng: -84.85299683, timestamp: 1402496134 },
{ lat: 27.47900009 , lng: -85.05599976, timestamp: 1402531513 },
{ lat: 28.08499908 , lng: -85.17300415, timestamp: 1402662222 },
{ lat: 28.36300087 , lng: -85.34298706, timestamp: 1402706110 },
{ lat: 28.18400002 , lng: -85.40701294, timestamp: 1402790785 },
{ lat: 28.65600014 , lng: -86.13800049, timestamp: 1402954319 },
{ lat: 28.78199959 , lng: -86.22000122, timestamp: 1403004581 },
{ lat: 29.07500076 , lng: -85.85101318, timestamp: 1403089834 },
{ lat: 29.02899933 , lng: -85.68099976, timestamp: 1403292498 },
{ lat: 28.94000053 , lng: -85.50900269, timestamp: 1403310041 },
{ lat: 28.43400002 , lng: -85.97399902, timestamp: 1403435723 },
{ lat: 28.50200081 , lng: -85.8460083, timestamp: 1403526421 },
{ lat: 28.51099968 , lng: -85.63400269, timestamp: 1403609854 },
{ lat: 28.8560009 , lng: -85.42199707, timestamp: 1403693989 },
{ lat: 28.97599983 , lng: -85.45800781, timestamp: 1404052643 },
{ lat: 28.04199982 , lng: -85.18301392, timestamp: 1404244975 },
{ lat: 27.10300064 , lng: -85.00900269, timestamp: 1404412840 },
{ lat: 26.9810009 , lng: -84.84298706, timestamp: 1404432838 },
{ lat: 26.6609993 , lng: -84.4460144, timestamp: 1404557408 },
{ lat: 26.93000031 , lng: -83.39498901, timestamp: 1404645117 },
{ lat: 24.79999924 , lng: -79.15499878, timestamp: 1405619466 },
{ lat: 26.63299942 , lng: -80.4079895, timestamp: 1405800205 },
{ lat: 29.39100075 , lng: -80.18099976, timestamp: 1405956504 },
{ lat: 32.32400131 , lng: -76.24301147, timestamp: 1406583366 },
{ lat: 33.21099854 , lng: -76.72900391, timestamp: 1406762543 },
{ lat: 33.07300186 , lng: -76.79901123, timestamp: 1406993520 },
{ lat: 33.44499969 , lng: -76.64099121, timestamp: 1407094998 },
{ lat: 33.60599899 , lng: -76.60598755, timestamp: 1407166611 },
{ lat: 33.22600174 , lng: -76.7170105, timestamp: 1407266696 },
{ lat: 33.25999832 , lng: -76.74798584, timestamp: 1407286507 },
{ lat: 33.88899994 , lng: -76.00500488, timestamp: 1407546956 },
{ lat: 34.45700073 , lng: -75.69699097, timestamp: 1407715897 },
{ lat: 34.22200012 , lng: -75.61999512, timestamp: 1407866858 },
{ lat: 33.7140007 , lng: -76.31399536, timestamp: 1408313704 },
{ lat: 33.56399918 , lng: -76.3210144, timestamp: 1408352060 },
{ lat: 33.44200134 , lng: -76.35198975, timestamp: 1408414504 },
{ lat: 33.10900116 , lng: -76.60900879, timestamp: 1408736412 },
{ lat: 33.03300095 , lng: -77.45599365, timestamp: 1408901500 },
{ lat: 33.10100174 , lng: -77.58099365, timestamp: 1409173504 },
{ lat: 35.81700134 , lng: -78.44999695, timestamp: 1409644555 },
{ lat: 35.35900116 , lng: -74.93701172, timestamp: 1409763350 },
{ lat: 35.2120018 , lng: -74.89898682, timestamp: 1409842746 },
{ lat: 35.00299835 , lng: -75.05599976, timestamp: 1410036330 },
{ lat: 35.04000092 , lng: -75.04699707, timestamp: 1410059796 },
{ lat: 35.24900055 , lng: -74.83999634, timestamp: 1410200108 },
{ lat: 35.43099976 , lng: -74.8039856, timestamp: 1410396156 },
{ lat: 35.31600189 , lng: -74.90301514, timestamp: 1410515044 },
{ lat: 35.30400085 , lng: -74.94799805, timestamp: 1410609656 },
{ lat: 35.51200104 , lng: -74.74099731, timestamp: 1410944290 },
{ lat: 35.68500137 , lng: -74.96398926, timestamp: 1411005490 },
{ lat: 36.86100006 , lng: -74.83599854, timestamp: 1411210354 },
{ lat: 38.06999969 , lng: -75.66101074, timestamp: 1411320235 },
{ lat: 38.25099945 , lng: -73.92999268, timestamp: 1411364847 },
{ lat: 39.04000092 , lng: -73.79598999, timestamp: 1411434154 },
{ lat: 39.88100052 , lng: -73.63800049, timestamp: 1411542250 },
{ lat: 40.44300079 , lng: -73.01400757, timestamp: 1411603326 },
{ lat: 40.75400162 , lng: -72.08898926, timestamp: 1411689788 },
{ lat: 41.0340004 , lng: -69.88400269, timestamp: 1411810915 },
{ lat: 41.52299881 , lng: -69.99798584, timestamp: 1412191497 },
{ lat: 41.45000076 , lng: -70.03100586, timestamp: 1412241206 },
{ lat: 41.85699844 , lng: -68.45098877, timestamp: 1412416908 },
{ lat: 41.53300095 , lng: -68.79598999, timestamp: 1412467882 },
{ lat: 41.95600128 , lng: -70.12298584, timestamp: 1412712606 },
{ lat: 41.91199875 , lng: -70.04699707, timestamp: 1412790245 },
{ lat: 41.60400009 , lng: -70.26000977, timestamp: 1412834362 },
{ lat: 41.7879982 , lng: -70.3380127, timestamp: 1413047271 },
{ lat: 42.11800003 , lng: -70.29699707, timestamp: 1413103606 },
{ lat: 42.12900162 , lng: -70.29199219, timestamp: 1413218921 },
{ lat: 42.05400085 , lng: -70.29199219, timestamp: 1413304477 },
{ lat: 41.74599838 , lng: -70.32598877, timestamp: 1413440509 },
{ lat: 41.96699905 , lng: -69.59100342, timestamp: 1413567622 },
{ lat: 41.81900024 , lng: -70.10998535, timestamp: 1413728718 },
{ lat: 41.86000061 , lng: -70.10998535, timestamp: 1413763535 },
{ lat: 42.13499832 , lng: -70.15701294, timestamp: 1413916924 },
{ lat: 41.38800049 , lng: -69.91799927, timestamp: 1414046990 },
{ lat: 41.57300186 , lng: -70.00900269, timestamp: 1414109986 },
{ lat: 41.92399979 , lng: -69.24700928, timestamp: 1414201463 },
{ lat: 41.54999924 , lng: -69.94400024, timestamp: 1415551671 },
{ lat: 41.5530014 , lng: -69.94500732, timestamp: 1416099065 },
{ lat: 40.95199966 , lng: -69.78799438, timestamp: 1416722586 },
{ lat: 41.26900101 , lng: -69.55899048, timestamp: 1417243626 },
{ lat: 41.82099915 , lng: -69.79299927, timestamp: 1417335706 },
{ lat: 41.43500137 , lng: -69.77200317, timestamp: 1417547510 },
{ lat: 41.55599976 , lng: -70.0249939, timestamp: 1417601307 },
{ lat: 41.41699982 , lng: -70.17401123, timestamp: 1418021024 },
{ lat: 41.54499817 , lng: -70.43899536, timestamp: 1418087728 },
{ lat: 41.09199905 , lng: -69.70599365, timestamp: 1419558744 },
{ lat: 40.8390007 , lng: -69.90100098, timestamp: 1419638406 },
{ lat: 41.41600037 , lng: -70.09399414, timestamp: 1419796226 },
{ lat: 41.4679985 , lng: -69.85198975, timestamp: 1419931290 },
{ lat: 40.53300095 , lng: -69.40600586, timestamp: 1420182498 },
{ lat: 39.99599838 , lng: -69.3710022, timestamp: 1420246264 },
{ lat: 39.47399902 , lng: -70.72698975, timestamp: 1420331123 },
{ lat: 38.36299896 , lng: -72.27700806, timestamp: 1420422615 },
{ lat: 37.84899902 , lng: -72.06399536, timestamp: 1420513556 },
{ lat: 37.38000107 , lng: -72.49700928, timestamp: 1420590446 },
{ lat: 37.11299896 , lng: -73.29499817, timestamp: 1420675490 },
{ lat: 36.10100174 , lng: -74.69699097, timestamp: 1420782636 },
{ lat: 35.72299957 , lng: -75.67498779, timestamp: 1420854316 },
{ lat: 35.04399872 , lng: -75.28100586, timestamp: 1421001177 },
{ lat: 34.62599945 , lng: -75.90301514, timestamp: 1421046275 },
{ lat: 34.52700043 , lng: -76.48599243, timestamp: 1421143588 },
{ lat: 32.74900055 , lng: -77.42700195, timestamp: 1421494183 },
{ lat: 32.90800095 , lng: -78.69699097, timestamp: 1421553771 },
{ lat: 32.53300095 , lng: -79.35400391, timestamp: 1421664057 },
{ lat: 32.35300064 , lng: -79.57598877, timestamp: 1421713509 },
{ lat: 30.68300056 , lng: -82.23999023, timestamp: 1422013148 },
{ lat: 30.59399986 , lng: -81.34298706, timestamp: 1422080521 },
{ lat: 29.88299942 , lng: -80.90899658, timestamp: 1422258163 },
{ lat: 29.91900063 , lng: -80.92098999, timestamp: 1422328930 },
{ lat: 29.33699989 , lng: -80.23098755, timestamp: 1422429732 },
{ lat: 29.04299927 , lng: -80.70901489, timestamp: 1422515791 },
{ lat: 28.93300056 , lng: -80.81900024, timestamp: 1422613168 },
{ lat: 28.96800041 , lng: -81.17401123, timestamp: 1423104438 },
{ lat: 30.86199951 , lng: -81.11999512, timestamp: 1423186933 },
{ lat: 35.26800156 , lng: -75.38699341, timestamp: 1424138154 },
{ lat: 35.1780014 , lng: -75.30300903, timestamp: 1424223320 },
{ lat: 34.51200104 , lng: -75.38600159, timestamp: 1424356690 },
{ lat: 33.86800003 , lng: -77.28799438, timestamp: 1424705944 },
{ lat: 33.46099854 , lng: -77.30999756, timestamp: 1424777838 },
{ lat: 34.0530014 , lng: -77.56199646, timestamp: 1425206927 },
{ lat: 34.95600128 , lng: -77.03900146, timestamp: 1425257685 },
{ lat: 33.83000183 , lng: -77.63500977, timestamp: 1425348696 },
{ lat: 34.27199936 , lng: -75.82099915, timestamp: 1425433698 },
{ lat: 35.43099976 , lng: -74.88101196, timestamp: 1425518928 },
{ lat: 36.18899918 , lng: -76.38200378, timestamp: 1425630881 },
{ lat: 35.04399872 , lng: -74.78700256, timestamp: 1425692255 },
{ lat: 34.16699982 , lng: -74.31700134, timestamp: 1425935193 },
{ lat: 34.14400101 , lng: -74.32699585, timestamp: 1425950526 },
{ lat: 33.42499924 , lng: -73.63699341, timestamp: 1426035721 },
{ lat: 32.31000137 , lng: -75.36499023, timestamp: 1426212097 },
{ lat: 33.26599884 , lng: -72.14498901, timestamp: 1426317916 },
{ lat: 33.39099884 , lng: -72.07800293, timestamp: 1426379946 },
{ lat: 33.50400162 , lng: -72.33099365, timestamp: 1426467893 },
{ lat: 34.1609993 , lng: -73.65600586, timestamp: 1426585742 },
{ lat: 34.16600037 , lng: -73.65200806, timestamp: 1426638667 },
{ lat: 34.63199997 , lng: -74.10099792, timestamp: 1426723414 },
{ lat: 34.89099884 , lng: -75.55799866, timestamp: 1426831892 },
{ lat: 35.63899994 , lng: -76.57199097, timestamp: 1426935526 },
{ lat: 35.36800003 , lng: -74.93800354, timestamp: 1427020006 },
{ lat: 35.36100006 , lng: -75.09700012, timestamp: 1427136180 },
{ lat: 35.19499969 , lng: -75.50299072, timestamp: 1427160958 },
{ lat: 34.18999863 , lng: -76.76699829, timestamp: 1427280599 },
{ lat: 35.25999832 , lng: -75.26599121, timestamp: 1427643173 },
{ lat: 35.66799927 , lng: -75.14001465, timestamp: 1428044366 },
{ lat: 30.31800079 , lng: -82.13900757, timestamp: 1430476454 },
{ lat: 30.37800026 , lng: -79.38600159, timestamp: 1430530305 },
{ lat: 30.30599976 , lng: -76.96600342, timestamp: 1430750951 },
{ lat: 30.16200066 , lng: -77.00799561, timestamp: 1430811823 },
{ lat: 29.95899963 , lng: -76.93200684, timestamp: 1430907929 },
{ lat: 29.68000031 , lng: -77.36099243, timestamp: 1430961682 },
{ lat: 28.75200081 , lng: -77.19699097, timestamp: 1431119802 },
{ lat: 34.00600052 , lng: -76.46600342, timestamp: 1435157322 },
{ lat: 34.92100143 , lng: -74.90200806, timestamp: 1435674157 },
{ lat: 35.07500076 , lng: -74.86801147, timestamp: 1436006953 },
{ lat: 34.82500076 , lng: -74.85998535, timestamp: 1436467247 },
{ lat: 34.77099991 , lng: -75.40499878, timestamp: 1436652280 },
{ lat: 34.69100189 , lng: -75.38598633, timestamp: 1436716865 },
{ lat: 34.68700027 , lng: -74.47900391, timestamp: 1436904683 },
{ lat: 34.56200027 , lng: -73.66699219, timestamp: 1436999346 },
{ lat: 33.76699829 , lng: -75.02099609, timestamp: 1437442193 },
{ lat: 33.69400024 , lng: -74.56100464, timestamp: 1437523865 },
{ lat: 34.9620018 , lng: -74.95401001, timestamp: 1438108140 },
{ lat: 35.08100128 , lng: -74.66699219, timestamp: 1438182264 },
{ lat: 35.70000076 , lng: -74.67300415, timestamp: 1438371366 },
{ lat: 35.72900009 , lng: -74.6000061, timestamp: 1438396483 },
{ lat: 35.70999908 , lng: -74.85998535, timestamp: 1438551232 },
{ lat: 35.61199951 , lng: -74.58401489, timestamp: 1438628307 },
{ lat: 35.73600006 , lng: -73.60501099, timestamp: 1438672947 },
{ lat: 35.37200165 , lng: -71.4079895, timestamp: 1438784499 },
{ lat: 34.99300003 , lng: -71.12799072, timestamp: 1438850541 },
{ lat: 33.88800049 , lng: -69.18099976, timestamp: 1439131343 },
{ lat: 34.75999832 , lng: -70.95199585, timestamp: 1439219751 },
{ lat: 35.01399994 , lng: -74.60998535, timestamp: 1439262890 },
{ lat: 35.52700043 , lng: -74.83699799, timestamp: 1440883948 },
{ lat: 35.39400101 , lng: -74.88300323, timestamp: 1441056193 },
{ lat: 35.31999969 , lng: -74.87200165, timestamp: 1441218401 },
{ lat: 35.30199814 , lng: -74.88300323, timestamp: 1441274198 },
{ lat: 35.96500015 , lng: -74.10600281, timestamp: 1441395716 },
{ lat: 35.83300018 , lng: -74.23699951, timestamp: 1441452359 },
{ lat: 35.5719986 , lng: -74.14499664, timestamp: 1441851080 },
{ lat: 35.37200165 , lng: -74.04299927, timestamp: 1441932657 },
{ lat: 35.74300003 , lng: -74.41999817, timestamp: 1442081659 },
{ lat: 35.69200134 , lng: -74.76499939, timestamp: 1442156941 },
{ lat: 35.38899994 , lng: -74.84200287, timestamp: 1442238905 },
{ lat: 35.60499954 , lng: -74.83699799, timestamp: 1442673783 },
{ lat: 35.5340004 , lng: -74.73400116, timestamp: 1442784528 },
{ lat: 35.4109993 , lng: -74.71700287, timestamp: 1442818067 },
{ lat: 35.31399918 , lng: -75.04000092, timestamp: 1443023043 },
{ lat: 36.19400024 , lng: -73.66999817, timestamp: 1443173227 },
{ lat: 36.95100021 , lng: -73.32700348, timestamp: 1443228357 },
{ lat: 36.625 , lng: -74.29299927, timestamp: 1443316133 },
{ lat: 36.2879982 , lng: -74.43199921, timestamp: 1443404722 },
{ lat: 34.34999847 , lng: -75.63700104, timestamp: 1444079126 },
{ lat: 34.54999924 , lng: -75.33300018, timestamp: 1444148209 },
{ lat: 34.43799973 , lng: -75.28500366, timestamp: 1444176948 },
{ lat: 31.54199982 , lng: -73.27200317, timestamp: 1444608972 },
{ lat: 31.22699928 , lng: -72.41799927, timestamp: 1444729731 },
{ lat: 30.56900024 , lng: -72.43199921, timestamp: 1445217204 },
{ lat: 31.04101944 , lng: -74.64627838, timestamp: 1445463780 },
{ lat: 31.04100037 , lng: -74.64600372, timestamp: 1445478191 },
{ lat: 31.74148941 , lng: -73.6663208, timestamp: 1445640300 },
{ lat: 31.74099922 , lng: -73.66600037, timestamp: 1445654730 },
{ lat: 31.92827988 , lng: -73.78208923, timestamp: 1445797020 },
{ lat: 32.14500046 , lng: -73.59799957, timestamp: 1445818929 },
{ lat: 33.61997986 , lng: -72.06922913, timestamp: 1445962440 },
{ lat: 33.61100006 , lng: -72.40799713, timestamp: 1445998882 },
{ lat: 34.27399826 , lng: -72.27700043, timestamp: 1446080448 },
{ lat: 34.8030014 , lng: -70.38899994, timestamp: 1446168691 },
{ lat: 37.9142189 , lng: -63.54116058, timestamp: 1446782580 },
{ lat: 37.95299911 , lng: -62.77000046, timestamp: 1446858912 },
{ lat: 39.41899872 , lng: -60.67699814, timestamp: 1446941155 },
{ lat: 39.53926849 , lng: -59.69176102, timestamp: 1447030920 },
{ lat: 39.18416977 , lng: -57.99123001, timestamp: 1447140600 },
{ lat: 39.58499908 , lng: -56.36899948, timestamp: 1447233418 },
{ lat: 40.34000015 , lng: -55.37099838, timestamp: 1447287255 },
{ lat: 40.81299973 , lng: -53.97000122, timestamp: 1447375932 },
{ lat: 41.33599854 , lng: -53.27899933, timestamp: 1447461038 },
{ lat: 42.07099915 , lng: -52.82799911, timestamp: 1447546224 },
{ lat: 42.34899902 , lng: -52.28900146, timestamp: 1447634801 },
{ lat: 43.10599899 , lng: -51.59700012, timestamp: 1447719876 },
{ lat: 42.99399948 , lng: -52.3370018, timestamp: 1447857829 },
{ lat: 43.47499847 , lng: -51.5340004, timestamp: 1447942799 },
{ lat: 41.86600113 , lng: -51.17499924, timestamp: 1448003373 },
{ lat: 42.54399872 , lng: -52.3730011, timestamp: 1448095032 },
{ lat: 43.13999939 , lng: -52.11399841, timestamp: 1448151940 },
{ lat: 44.01399994 , lng: -52.35200119, timestamp: 1448239660 },
{ lat: 45.02600098 , lng: -51.63700104, timestamp: 1448353115 },
{ lat: 44.30099869 , lng: -51.43700027, timestamp: 1448413146 },
{ lat: 43.23899841 , lng: -51.66799927, timestamp: 1448498334 },
{ lat: 42.43000031 , lng: -51.49700165, timestamp: 1448689312 },
{ lat: 42.34799957 , lng: -50.56000137, timestamp: 1448781124 },
{ lat: 43.55400085 , lng: -51.97600174, timestamp: 1448879341 },
{ lat: 43.27299881 , lng: -53.04700089, timestamp: 1448932786 },
{ lat: 42.81900024 , lng: -54.10200119, timestamp: 1449038362 },
{ lat: 42.82300186 , lng: -56.15100098, timestamp: 1449103107 },
{ lat: 43.19499969 , lng: -55.19800186, timestamp: 1449209434 },
{ lat: 44.13100052 , lng: -56.19200134, timestamp: 1449279768 },
{ lat: 44.82799911 , lng: -54.7120018, timestamp: 1449380939 },
{ lat: 44.98799896 , lng: -53.57400131, timestamp: 1449446825 },
{ lat: 45.54999924 , lng: -53.48799896, timestamp: 1449623466 },
{ lat: 45.20999908 , lng: -53.26300049, timestamp: 1449708542 },
{ lat: 43.94499969 , lng: -54.15499878, timestamp: 1449809987 },
{ lat: 42.61399841 , lng: -53.74599838, timestamp: 1449910831 },
{ lat: 41.9469986 , lng: -53.52799988, timestamp: 1450010724 },
{ lat: 41.22100067 , lng: -53.15100098, timestamp: 1450073252 },
{ lat: 41.48699951 , lng: -51.27799988, timestamp: 1450266511 },
{ lat: 41.68899918 , lng: -51.59000015, timestamp: 1450310748 },
{ lat: 42.56399918 , lng: -52.8409996, timestamp: 1450421975 },
{ lat: 43.11899948 , lng: -52.30400085, timestamp: 1450483898 },
{ lat: 44.3730011 , lng: -53.13800049, timestamp: 1450599230 },
{ lat: 44.42499924 , lng: -53.54000092, timestamp: 1450947898 },
{ lat: 42.95600128 , lng: -55.84500122, timestamp: 1451136087 },
{ lat: 43.14099884 , lng: -55.69499969, timestamp: 1451176695 },
{ lat: 44.45700073 , lng: -54.80199814, timestamp: 1451293684 },
{ lat: 45.33499908 , lng: -54.69300079, timestamp: 1451364709 },
{ lat: 45.30400085 , lng: -53.81700134, timestamp: 1451450814 },
{ lat: 44.53900146 , lng: -54.09199905, timestamp: 1451645036 },
{ lat: 43.40499878 , lng: -55.13399887, timestamp: 1451719716 },
{ lat: 42.39300156 , lng: -55.34999847, timestamp: 1451938958 },
{ lat: 41.97299957 , lng: -55.53499985, timestamp: 1452072261 },
{ lat: 42.56999969 , lng: -56.14699936, timestamp: 1452421324 },
{ lat: 37.73199844 , lng: -59.27199936, timestamp: 1452472592 },
{ lat: 44.90399933 , lng: -55.20000076, timestamp: 1452557675 },
{ lat: 52.11000061 , lng: -53.98400116, timestamp: 1453209745 },
{ lat: 41.33200073 , lng: -54.99100113, timestamp: 1453276433 },
{ lat: 40.79399872 , lng: -54.52799988, timestamp: 1453336031 },
{ lat: 40.98799896 , lng: -54.53499985, timestamp: 1453537230 },
{ lat: 40.79999924 , lng: -54.84500122, timestamp: 1453611621 },
{ lat: 42.51699829 , lng: -54.40299988, timestamp: 1453703294 },
{ lat: 43.50400162 , lng: -55.29999924, timestamp: 1453773711 },
{ lat: 44.34500122 , lng: -54.42599869, timestamp: 1453852916 },
{ lat: 44.78300095 , lng: -55.68099976, timestamp: 1454303682 },
{ lat: 45.12799835 , lng: -55.7820015, timestamp: 1454461137 },
{ lat: 42.33300018 , lng: -62.63499832, timestamp: 1454620000 },
{ lat: 43.0530014 , lng: -58.29700089, timestamp: 1454634568 },
{ lat: 42.22200012 , lng: -57.36600113, timestamp: 1454722771 },
{ lat: 42.87400055 , lng: -58.26100159, timestamp: 1454824191 },
{ lat: 42.74300003 , lng: -58.31900024, timestamp: 1454889767 },
{ lat: 41.90800095 , lng: -56.99499893, timestamp: 1454995083 },
{ lat: 42.23699951 , lng: -54.95700073, timestamp: 1455063457 },
{ lat: 42.77999878 , lng: -52.72100067, timestamp: 1455208138 },
{ lat: 43.32600021 , lng: -52.29299927, timestamp: 1455239611 },
{ lat: 44.72800064 , lng: -54.91400146, timestamp: 1455610784 },
{ lat: 43.68500137 , lng: -55.5340004, timestamp: 1456219057 },
{ lat: 42.80699921 , lng: -58.18600082, timestamp: 1456273279 },
{ lat: 41.67100143 , lng: -58.85699844, timestamp: 1456359524 },
{ lat: 40.74900055 , lng: -58.60200119, timestamp: 1456465015 },
{ lat: 40.50299835 , lng: -59.04299927, timestamp: 1456567996 },
{ lat: 39.4090004 , lng: -58.37900162, timestamp: 1456654253 },
{ lat: 33.81499863 , lng: -64.16500092, timestamp: 1457638276 },
{ lat: 33.88700104 , lng: -64.22000122, timestamp: 1457656975 },
{ lat: 34.05500031 , lng: -64.80500031, timestamp: 1457744978 },
{ lat: 34.59199905 , lng: -66.57499695, timestamp: 1457880524 },
{ lat: 35.01800156 , lng: -67.72799683, timestamp: 1457981041 },
{ lat: 35.2519989 , lng: -68.35099792, timestamp: 1458036070 },
{ lat: 36.4129982 , lng: -68.38500214, timestamp: 1458097729 },
{ lat: 36.87099838 , lng: -68.47599792, timestamp: 1458177051 },
];
